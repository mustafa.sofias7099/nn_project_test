

import torchvision
import torch
from torch import nn, optim
from torchvision import models, datasets, transforms
import numpy as np
from matplotlib import pyplot as plt
import json
from PIL import Image


mean_vals = [0.485, 0.456, 0.406]
std_vals = [0.229, 0.224, 0.225]

fl_pth_train = r"C:\Users\musta\Documents\train"
fl_pth_test = r"C:\Users\musta\Documents\test"
img_transforms = transforms.Compose([transforms.RandomResizedCrop(224),transforms.ToTensor(), 
                                     transforms.Normalize(mean=mean_vals,std=std_vals)])
img_dataset = datasets.ImageFolder(fl_pth_train, transform = img_transforms)


img_loader = torch.utils.data.DataLoader(img_dataset, batch_size=16)
test_img_transforms = transforms.Compose([transforms.RandomResizedCrop(224), transforms.ToTensor(),
                                          transforms.Normalize(mean = mean_vals, std = std_vals)])

test_img_dataset = datasets.ImageFolder(fl_pth_test, transform=test_img_transforms)


test_img_loader = torch.utils.data.DataLoader(test_img_dataset, batch_size = 16, shuffle=True)




img_prcs_mdl = models.vgg16(pretrained=True)

with open(r"E:\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\cat_to_name.json", 'r') as file:
    categ_clss = json.load(file)

img_prcs_clss = nn.Sequential(nn.Linear(25088, 1568), nn.ReLU(), nn.Dropout(0.2),
                              nn.Linear(1568, 392), nn.ReLU(), nn.Dropout(0.2),
                              nn.Linear(392, 98), nn.ReLU(), nn.Dropout(0.2),
                              nn.Linear(98, 24), nn.ReLU(), nn.Dropout(0.2),
                              nn.Linear(24, 3), nn.LogSoftmax(dim=1))

img_prcs_mdl.classifier = img_prcs_clss
nn_mdl_lss_func = nn.NLLLoss()
nn_mdl_optim = optim.SGD(img_prcs_mdl.classifier.parameters(), lr=0.003)

epochs = 2
for i in range(epochs):
    ttl_lss = 0
    for img, label in img_loader:
        nn_mdl_optim.zero_grad()

        prcsd_img = img_prcs_mdl(img)

        loss_val = nn_mdl_lss_func(prcsd_img, label)

        loss_val.backward()
        nn_mdl_optim.step()

        ttl_lss += loss_val.item()

    else:

        ttl_test_lss = 0
        ttl_crct = 0
        train_lss = ttl_lss/len(img_loader.dataset)
        print('Training Loss: {:4f}'.format(train_lss))
    img_prcs_mdl.train()

'''
    ▀Implementing validation
'''
test_loss = 0
accuracy = 0
with torch.no_grad():
    img_prcs_mdl.eval()
    for img, lbl in test_img_loader:
        log_vals = img_prcs_mdl(img)
        test_loss += nn_mdl_lss_func(log_vals, lbl)

        probe_vals = torch.exp(log_vals)
        top_p, top_c = probe_vals.topk(2, dim=1)
        label_equalisation = top_c == lbl.view(top_c.shape[0],-1)
        accuracy += torch.mean(label_equalisation.type(torch.FloatTensor))

        print('Testing accuracy = {:4f}'.format(accuracy/len(test_img_loader)))

def pred(img_pth, model, topk = 2):
    img = Image.open(img_pth)

    data_transforms = transforms.Compose([transforms.RandomResizedCrop(224), 
                                          transforms.ToTensor(), 
                                          transforms.Normalize(mean=mean_vals, std = std_vals)])
    img_tnsr = data_transforms(img)

    img_tnsr.unsqueeze_(0)

    probe_vals = torch.exp(model(img_tnsr))

    top_probe, top_clss = probe_vals.topk(topk, dim=1)
    
    return top_clss


x = pred(r"E:\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\train\6\image_07163.jpg",model=img_prcs_mdl)
print(categ_clss[str(x.numpy()[0,0])])