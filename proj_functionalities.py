
# رب إجعلني مقيم الصلوة و لأمرك أمتثل دائماً, إنك قوي كريم مجيب الدعاء

import torchvision
import torch
from torch import nn, optim
from torchvision import models, datasets, transforms
import numpy as np
from matplotlib import pyplot as plt
import json
from PIL import Image

mean_vals = [0.485, 0.456, 0.406]
std_vals = [0.229, 0.224, 0.225]

fl_pth = "E:\\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\\train"

train_img_transforms = transforms.Compose([transforms.RandomResizedCrop(50),transforms.ToTensor(), 
                                    transforms.Normalize(mean=mean_vals,std=std_vals)])

img_dataset = datasets.ImageFolder(fl_pth, transform=train_img_transforms)

img_loader = torch.utils.data.DataLoader(img_dataset, batch_size=16)

image, label = next(iter(img_loader))

img_prcs_mdl = models.vgg16(pretrained=True)

img_clss = nn.Sequential(nn.Linear(25088, 6272),nn.ReLU(), nn.Dropout(0.2),
                         nn.Linear(6272, 1568), nn.ReLU(), nn.Dropout(0.2),
                         nn.Linear(1568, 392), nn.ReLU(), nn.Dropout(0.2),
                         nn.Linear(392, 98), nn.ReLU(), nn.Dropout(0.2),
                         nn.Linear(98,24), nn.ReLU(), nn.Dropout(0.2),
                         nn.Linear(24, 10), nn.LogSoftmax(dim=1))

img_prcs_mdl.classifier = img_clss

loss_func = nn.NLLLoss()

ntwrk_optim = optim.SGD(img_prcs_mdl.classifier.parameters(), lr=0.003)

epochs = 1
for i in range(epochs):
    ttl_lss = 0
    for img, sign in img_loader:
        ntwrk_optim.zero_grad()
        output_img = img_prcs_mdl(img)

        loss_val = loss_func(output_img, sign)
        loss_val.backward()
        ntwrk_optim.step()

        ttl_lss += loss_val.item()
    else:
        train_loss = ttl_lss/len(img_loader.dataset)
        print('Training Loss: {:4f}'.format(train_loss))
ttl_test_lss = 0
ttl_crct = 0
with torch.no_grad():
    for img, lbl in img_loader:
        prob_img = img_prcs_mdl(img)
        tst_lss = loss_func(prob_img, lbl)
        ttl_test_lss += tst_lss.item()

        probe_val = torch.exp(prob_img)
        top_p, top_class = probe_val.topk(5, dim=1)
    # >>>>>••••• Fault Retutn to the project.... <top_class.shape[0],-1>
        label_equalisation = top_class == lbl.view(top_class.shape[0],-1)
        ttl_crct += label_equalisation.sum().item()

    # >>>>>••••• Fault Retutn to the project.... <loader object//test_loader on the project case>
    train_loss = ttl_lss/len(img_loader.dataset)

    print('Training Loss: {:4f}'.format(train_loss))
    print('Testing accuracy = {:4f}'.format(ttl_crct/len(img_loader.dataset)))

# JSON FILE Loader
with open("E:\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\\cat_to_name.json", 'r') as f:
    cat_to_name = json.load(f)

def process_image(image):
    ''' Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    '''
    img_org = Image.open(image)
    mean_vals = [0.485, 0.456, 0.406]
    std_vals = [0.229, 0.224, 0.225]
    img_processes = transforms.Compose([transforms.Resize(224),transforms.CenterCrop(224),transforms.ToTensor(),
                                        transforms.Normalize(mean=mean_vals, std=std_vals)])
    img_tnsr = img_processes(img_org)
    
    # Apply the image into the model
    img_tnsr.unsqueeze_(0)
    cnvrtd_img = img_clss(img_tnsr)
    # convert the tensor form image into a numpy array
    return cnvrtd_img.data.numpy().argmax()


def imshow(image, ax=None, title=None):
    """Imshow for Tensor."""
    if ax is None:
        fig, ax = plt.subplots()
    
    # PyTorch tensors assume the color channel is the first dimension
    # but matplotlib assumes is the third dimension
    image = image.numpy().transpose((1, 2, 0))
    
    # Undo preprocessing
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    image = std * image + mean
    
    # Image needs to be clipped between 0 and 1 or it looks like noise when displayed
    image = np.clip(image, 0, 1)
    
    ax.imshow(image)
    
    return ax


def predict(image_path, model, topk=5):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    # >>>>>••••• Fault Return to the project.... <set the structure to eval()>
    
    data_transforms = transforms.Compose([transforms.RandomResizedCrop(224), 
                                           transforms.ToTensor(), 
                                           transforms.Normalize(mean=mean_vals, std=std_vals)])
    img_dataset = datasets.ImageFolder(image_path,transform=data_transforms)
    img_dataset_idx = img_dataset.class_to_idx
    data_loader = torch.utils.data.DataLoader(img_dataset_idx, batch_size=48)
    images, labels = next(iter(data_loader))
    probe_log_vals = torch.exp(model(images))
    
    top_pic, top_class = probe_log_vals.topk(topk, dim=1)
    
    return top_class

    # >>>>>••••• Fault Return to the project.... <setting the model into models.vgg16(pretrained=True) as a parameter while calling>

print(predict(r"E:\important_files_py\Python Scripts\Artificial_Intelligence\AI_Programming_NanoDegree\train",model=models.vgg16(pretrained=True)))